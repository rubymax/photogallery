class GalleriesController < ApplicationController

  before_action :authenticate_user!, only: :create

  def index
    @galleries = Gallery.order(created_at: :desc)
    @gallery = Gallery.new
  end

  def create
    @gallery = current_user.galleries.build(photogallery_params)
    if @gallery.save
      redirect_to root_path
    else
      render 'index'
    end
  end

  private

  def photogallery_params
    params.require(:gallery).permit(:title, :image)
  end
end
