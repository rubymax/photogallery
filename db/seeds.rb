# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

5.times do
  AdminUser.create!(email: Faker::Internet.email, password: 'password', password_confirmation: 'password')
end


users = []
5.times do
  name = Faker::Name.name
  users.push User.create(name: name, email: Faker::Internet.email(name), password: 'password', password_confirmation: 'password')
end

# 10.times do
#   Gallery.create(image: File.new(fixtures_path.join('gallery.jpg')), title: Faker::Lorem.sentence, user: users.sample)
# end
